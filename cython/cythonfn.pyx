import numpy as np
cimport numpy as np

def calculate_z(int maxiter, zs, cs):
    """Calculate output list using Julia update rule"""
    cdef unsigned int i, n
    cdef double complex z, c
    cdef int[:] output = np.empty(len(zs), dtype=np.int32)

    for i in range(len(zs)):
        n = 0
        z = zs[i]
        c = cs[i]
        #a way to optimize the while cycle (which takes almost 36% of this function's time
        #we can change the order of the and verificaton, because n < maxiter is faster
        #than the abs value checking!
        #while abs(z) < 2 and n < maxiter:
        while n < maxiter and (z.real * z.real + z.imag * z.imag) < 4:
            z = z * z + c
            n += 1
        output[i] = n
    return output
