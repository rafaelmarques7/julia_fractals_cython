This is a project done for learning purposes, in which the goal was to learn the basics about Cython.

More than that, this project was very insightfull on how to **optimize program speed**, but even more important, on **how to find bottlenecks**.